/**
 *
 * Submitted by Jye Dewar (21488841) and Jake Nelson (20752958)
 *
 */

import java.util.*;

public class Between{

  // Store the adjacency list
  private static HashMap<Integer, HashSet<Integer>> adj = null;
  // Store the shortest route for any node i to any other node j in a matrix
  Integer [][] distance;
  // Store the number of paths which pass through nodes
  double [] cb;
  int n;

  public Between(HashMap<Integer, HashSet<Integer>> adj){
    this.adj = adj;
    this.n = adj.size();
  }

  /*
  * Ulrik Brandes algorithm for Betweenness centrality implemented from research paper available at:
  * http://algo.uni-konstanz.de/publications/b-fabc-01.pdf
  */

  public void calculate(){
    cb = new double[n];
    distance = new Integer[n][n];

    for (int s = 0; s < n; s++) {

      Stack<Integer> stack = new Stack<Integer>();
      ArrayList<ArrayList<Integer>> paths = new ArrayList<ArrayList<Integer>>();
      for (int i = 0; i < n; i++){
        paths.add(new ArrayList<Integer>());
        distance[s][i] = -1;
      }

      double[] sigma = new double[n];

      sigma[s] = 1;
      distance[s][s] = 0;

      Queue<Integer> queue = new LinkedList<Integer>();

      queue.add(s);

      while(!queue.isEmpty()) {

        int v = queue.remove();
        stack.push(v);

        for (Integer w : adj.get(v)){
          if ((int)distance[s][w] < 0)
          {
            queue.add(w);
            distance[s][w] = distance[s][v] + 1;
          }
          if (distance[s][w] == distance[s][v] + 1)
          {
            sigma[w] += sigma[v];
            paths.get(w).add(v);
          }
        }
      }

      double delta[] = new double[n];

      while (!stack.isEmpty()){
        int w = stack.pop();
        for (int vindex = 0; vindex < paths.get(w).size(); vindex++){
            delta[paths.get(w).get(vindex)] += ( sigma[paths.get(w).get(vindex)]/sigma[w] ) * (1 + delta[w]);
        }
        if (w != s){
          cb[w] += delta[w];
        }
      }
    }
  }

  // Returns the integer array of distance for each node to every other node
  public Integer[][] getDistance(){
      if (distance == null){
        calculate();
      }
      return distance;
  }

  // Returns the double array containing the value of paths passing through each node
  public double[] getNumPaths(){
      if (distance == null){
        calculate();
      }
      return cb;
  }
}
