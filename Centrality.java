/**
 *
 * Submitted by Jye Dewar (21488841) and Jake Nelson (20752958)
 *
 */

import java.util.*;
import java.io.*;
import java.lang.Math.*;

/**
 * Calculates and displays various methods of centrality.
 **/
public class Centrality {

  // Storing the adjacency list as a HashMap for speed of degree centrality
  private static HashMap<Integer, HashSet<Integer>> adj = null;
  // Storing a map of ID's to the corresponding value for the row of the graph
  private static HashMap<Integer, Integer> imap = null;
  private static HashMap<Integer, Integer> nmap = null;
  // Storing the graph represetation as an int matrix
  private static Integer[][] distance;
  private static double [] numPaths;

  private static int n = 0;
  private static int m = 0;

  /**
   * Prints the degree centrality of the graph given to the top 5 places
   **/
  public static void degree(){

    // Create a new array to store the max number of outward edges seen
    int[][] max = new int[m][2];

    // Int to store the current entries number of edges
    int current;

    // Iterate through the entries in the adjacency hash map and return the max
    for (Map.Entry<Integer, HashSet<Integer>> entry : adj.entrySet())
    {
      current = entry.getValue().size();
      for (int i = 0; i < m; i++)
      {
        if (current >= max[i][1])
        {
          // Start from the back of the max array and move forward
          for (int j = m-1; j > i; j--)
          {
            max[j][0] = max[j-1][0];
            max[j][1] = max[j-1][1];
          }

          // Set the new max
          max[i][1] = current;
          max[i][0] = entry.getKey();
          break;

        }
      }
    }

    // Display the top 5 and print
    String display = "\nDEGREE CENTRALITY\n";
    String formatted;
    for (int i = 0; i < m; i++)
    {
        formatted = String.format("%-10s :  %-5s \n", nmap.get(max[i][0]), max[i][1]);
        display += formatted;
    }
    System.out.println(display);

  }

  public static void calculate(){
    // Betweenness b = new Betweenness(graph);
    Between b = new Between(adj);
    b.calculate();
    numPaths = b.getNumPaths();
    distance = b.getDistance();

  }

  public static void closeness(){
    double sum;
    double[] closeness = new double[n];

    // Create a new array to store the max closeness seen
    double[][] max = new double[m][2];

    for (int i = 0; i < n; i++)
    {
      sum = 0;
      for (int j = 0; j < n; j++)
      {
        sum += distance[i][j];
      }
      double close = 1.0/sum;
      closeness[i] = close;

      for (int j = 0; j < m; j++)
      {
        if (close >= max[j][1])
        {
          // Start from the back of the max array and move forward
          for (int k = m-1; k > j; k--)
          {
            max[k][0] = max[k-1][0];
            max[k][1] = max[k-1][1];
          }

          // Set the new max
          max[j][1] = close;
          max[j][0] = i;
          break;

        }
      }
    }

    // Display the top 5 and print
    String display = "\nCLOSENESS CENTRALITY\n";
    String formatted;
    for (int i = 0; i < m; i++)
    {
        int x = nmap.get((int)max[i][0]);
        formatted = String.format("%-10s :  %-5s \n", x , max[i][1]);
        display += formatted;
    }
    System.out.println(display);

  }

  public static void betweenness(){

    // O(mV) + EV^2

    // Create a new array to store the max number of outward edges seen
    double[][] max = new double[m][2];

    // Int to store the current entries number of paths through
    double current;

    // Iterate through the entries in the adjacency hash map and return the max
    for (int i = 0; i < n; i++)
    {
      current = numPaths[i];
      for (int j = 0; j < m; j++)
      {
        if (current >= max[j][1])
        {
          // Start from the back of the max array and move forward
          for (int k = m-1; k > j; k--)
          {
            max[k][0] = max[k-1][0];
            max[k][1] = max[k-1][1];
          }

          // Set the new max
          max[j][1] = current;
          max[j][0] = i;
          break;

        }
      }
    }

    // Display the top 5 and print
    String display = "\nBETWEENNESS CENTRALITY\n";
    String formatted;
    double result = 0;
    for (int i = 0; i < m; i++)
    {
        int x = nmap.get((int)max[i][0]);
        formatted = String.format("%-10s :  %-5s \n", x , max[i][1]);
        display += formatted;
    }
    System.out.println(display);

  }

  public static void katz(double kval){

    double score;
    double[] katzness = new double[n];
    double[][] max = new double[m][2];

    for (int i = 0; i < n; i++)
    {
      score = 0;
      for (int j = 0; j < n; j++)
      {
        // Need to multiply the number of edges incident by the kval to the power of distance
        score += adj.get(j).size() * Math.pow(kval,distance[i][j]);
      }
      katzness[i] = score;
      for (int j = 0; j < m; j++)
      {
        if (score >= max[j][1])
        {
          // Start from the back of the max array and move forward
          for (int k = m-1; k > j; k--)
          {
            max[k][0] = max[k-1][0];
            max[k][1] = max[k-1][1];
          }

          // Set the new max
          max[j][1] = score;
          max[j][0] = i;
          break;

        }
      }
    }

    // Display the top 5 and print
    String display = "\nKATZ CENTRALITY\n";
    String formatted;
    for (int i = 0; i < m; i++)
    {
        int x = nmap.get((int)max[i][0]);
        formatted = String.format("%-10s :  %-5s \n", x , max[i][1]);
        display += formatted;
    }
    System.out.println(display);

  }

  /*
  * Reads in the graph from a text file where each edge is described one on
  * each line in the format:
  * source target
  * Complexity of read() is O(E) where E is the number of vertices
  */
  public static void read(String filename){

    adj = new HashMap<Integer, HashSet<Integer>>();
    nmap = new HashMap<Integer, Integer>();
    imap = new HashMap<Integer, Integer>();
    // adjb = new HashMap<Integer, HashSet<Integer>>();

    File input = new File (filename);
    try {

      Scanner sc = new Scanner(input);
      String string;
      String[] strings;
      Integer first, second;
      HashSet<Integer> temp;

      Integer count = 0;

      while (sc.hasNextLine()) {
          string = sc.nextLine();
          strings = string.split(" ");
          first = Integer.parseInt(strings[0]);
          second = Integer.parseInt(strings[1]);

          if (!imap.containsKey(first))
          {
            nmap.put(count, first);
            imap.put(first,count);
            temp = new HashSet<Integer>();
            adj.put(count, temp);
            count++;
          }

          // Add any vertices that don't have outward edges
          if (!imap.containsKey(second))
          {
            nmap.put(count, second);
            imap.put(second,count);
            temp = new HashSet<Integer>();
            adj.put(count, temp);
            count++;
          }

          first = imap.get(first);
          second = imap.get(second);

          // Add all vertices and their edges to the Hashmap storing the adjacency matrix
          temp = adj.get(first);
          if (!temp.contains(second))
          {
            temp.add(second);
            adj.put(first, temp);
          }
          temp = adj.get(second);
          if (!temp.contains(first))
          {
            temp.add(first);
            adj.put(second, temp);
          }

      }

      sc.close();

    } catch (Exception e) {
      e.printStackTrace();
    }

    n = adj.size();

  }

  public static void main(String[] args){

    if (args.length != 3)
    {
      System.out.println("\nUSAGE:\njava Centrality [filename] [katz constant] [number of top values]\ni.e.\njava Centrality 1.txt 0.5 5\n");
      // System.out.println("\nPlease give the first argument as the name of the text file describing the graph, and the second argument for the alpha Katz value.\n i.e. java Centrality 1.txt 0.5\n");
      return;
    }

    m = Integer.parseInt(args[2]);

    // Read in text file describing graph
    read(args[0]);

    // Print degree centrality
    degree();

    // Calculate all shortest paths
    calculate();

    // Print closeness centrality
    closeness();

    // Print betweenness centrality
    betweenness();

    // Print Katz centrality
    katz(Double.parseDouble(args[1]));

  }

}
