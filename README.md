# CITS2200 Project Usage

When this folder is downloaded/cloned, then the project can be compiled by typing the following commands into a terminal while in the folder:

```
rm *.class
javac Centrality.java
```

The program accepts three arguments; the filename to be read, the Katz centrality alpha value to use and the number of top values to display.

```
java Centrality [filename] [Katz coefficient] [number of top values to display]

java Centrality 1.txt 0.5 5
```
